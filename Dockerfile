FROM python:3.6-alpine

RUN mkdir -p /usr/src/bot
WORKDIR /usr/src/bot

COPY . /usr/src/bot

RUN apk add --update --no-cache python3-dev build-base libevent-dev gcc musl-dev libc-dev libffi-dev postgresql-dev
RUN pip install --no-cache-dir -r requirements.txt
