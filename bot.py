from permissions import only_superuser
from telebot import types
from keyboards import *
from sql.api import *
import telebot 
import re
import os
from bot_config import bot



@bot.message_handler(commands = ["start", "старт"])
def hello_user(message):
    CreateUser(chat_id = message.chat.id, name = message.from_user.first_name)
    bot.send_message(message.chat.id,
                    "Привет! Установи программу и начни тренировку",
                    reply_markup=user_kb())


@bot.message_handler(commands = [f"sudo:{os.getenv('TELEBOT_TOKEN')}"])
def hello_superuser(message):
    AddSuperUser(chat_id = message.chat.id)
    bot.send_message(message.chat.id, "Вы стали админом! Используйте команду /admin")


@bot.message_handler(commands = ["admin"])
@only_superuser
def hello_admin(message):
    bot.send_message(
                message.chat.id,
                "Добро пожаловать в админский режим!",
                reply_markup=admin_kb())


@bot.message_handler(func = lambda message: True and 'header:add_ex_to_program' in message.text, content_types = ['text'])
@only_superuser
def add_ex_to_program(message):
    data_list = re.findall(r'\w+:<(.+)>', message.text)
    text = AddExToProgram(data_list)
    bot.send_message(message.chat.id, text)
    

@bot.message_handler(func = lambda message: True and 'new_program:' in message.text, content_types = ['text'])
@only_superuser
def new_program_create(message):
    program_name = message.text.split(":")[1]
    if CreateNewProgram(program_name):
        bot.send_message(message.chat.id, f"Создана программа: {program_name}")
    else:
        bot.send_message(message.chat.id, f"Уже существует")


@bot.message_handler(func = lambda message: True and 'delete_program:' in message.text, content_types = ['text'])
@only_superuser
def delete_program(message):
    program_name = message.text.split(":")[1]
    if DeleteProgram(program_name):
        bot.send_message(message.chat.id, f"Удалена программа: {program_name}")
    else:
        bot.send_message(message.chat.id, f"Не существует")


@bot.message_handler(func = lambda message: True and 'new_ex:' in message.text, content_types = ['text'])
@only_superuser
def new_ex_template_create(message):
    ex_name = message.text.split(":")[1]
    if CreateNewExTemplate(ex_name):
        bot.send_message(message.chat.id, f"Создано упражнение: {ex_name}")
    else:
        bot.send_message(message.chat.id, f"Уже существует")


@bot.message_handler(func = lambda message: True and 'detele_ex_template:' in message.text, content_types = ['text'])
@only_superuser
def delete_ex_template(message):
    ex_name = message.text.split(":")[1]
    text = DeleteExTemplate(ex_name)
    bot.send_message(message.chat.id, text)


@bot.message_handler(func = lambda message: True and message.text == '🔎Установить новую программу🔎', content_types = ['text'])
def find_program(message):
    bot.send_message(message.chat.id, "Программы:", reply_markup = find_program_kb())


@bot.message_handler(func = lambda message: True and message.text == '🏋️️️️️️️️️️️️️Начать тренировку🏋️️️️️️️️️️', content_types = ['text'])
def go_train(message):
    program, days = GoTrain(message.chat.id)
    if program:
        bot.send_message(message.chat.id, 
        program.name,
        reply_markup = split_by_days_kb("getprogram", program, days))
    else:
        bot.send_message(message.chat.id, "У тебя нет программы\nВыбери программу")
        find_program(message)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message:
        if "setprogram" in call.data:
            program_id = call.data.split('_')[1]
            program_name = SetProgram(chat_id = call.message.chat.id, program_id = program_id)
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = f"Установлена программа: {program_name}")
        
        elif "getprogram" in call.data:
            data = call.data.split('_')
            program_id = data[1]
            day = data[2]
            program, exies = GetTextDay(program_id, day)

            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = f"Программа: {program.name}\nДень: {day}")
            
            for index, elem in enumerate(exies):
                bot.send_message(call.message.chat.id,
                    f"Упражнение #{index+1}\n\n{elem.ex_template.name}\n{elem.text}")
        
        elif "showdaysprogram" in call.data:
            data = call.data.split('_')
            program_id = data[1]
            program, days = ShowDaysProgram(program_id)   
            bot.delete_message(chat_id = call.message.chat.id, message_id = call.message.message_id)
            bot.send_message(
                call.message.chat.id,
                f"{program.name}\nВыбери день в котором будем удалять упражнения в программе",
                reply_markup = split_by_days_kb("removeexinprogram", program, days)
            )

        elif "removeexinprogram" in call.data:
            data = call.data.split("_")
            program_id = data[1]
            day = data[2]
            program, exies = GetTextDay(program_id, day)            
            bot.delete_message(chat_id = call.message.chat.id, message_id = call.message.message_id)
            bot.send_message(
                call.message.chat.id,
                f"{program.name}\nВыбери упражнение, которое нужно удалить",
                reply_markup = delete_ex_keyboard(program, exies)
            )
        
        elif "deleteex" in call.data:
            data_list = call.data.split("_")
            DeleteExFromProgram(data_list)
            bot.delete_message(chat_id = call.message.chat.id, message_id = call.message.message_id)
            bot.send_message(
                call.message.chat.id,
                "УПРАЖНЕНИЕ УДАЛЕНО ОТСЮДА"
            )
            
        elif "Список всех пользователей" in call.data:
            text = ''
            for index, user in enumerate(UsersList()):
                program = user.program.name if user.program else None
                text +=f"{index}: {user.fullname} {user.chat_id} {program}\n"
            bot.edit_message_text(
                chat_id = call.message.chat.id, 
                message_id = call.message.message_id,
                text = text)

        elif "Список всех упражнений" in call.data:
            text = ''
            for index, ex_template in enumerate(ExList()):
                text +=f"{index}:{ex_template.name}\n"
            bot.edit_message_text(
                chat_id = call.message.chat.id, 
                message_id = call.message.message_id,
                text = text)

        elif "Удалить мою программу" == call.data:
            DeleteMyProgram(call.message.chat.id)
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = f"У тебя больше нет программы")
        
        elif "Создать новую программу" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = "Чтобы создать новую программу\nПиши:")
            bot.send_message(call.message.chat.id, 'new_program:Имя программы')

        elif "Cоздать новое упражнение" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = "Чтобы создать новое упражнение\nПиши: ")
            bot.send_message(call.message.chat.id, 'new_ex:Имя упражнения')

        elif "Удалить программу️" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id, 
                message_id = call.message.message_id,
                text = "Чтобы удалить программу\nПиши:")
            bot.send_message(call.message.chat.id, 'delete_program:Имя программы')            

        elif "Добавить упражнение к программе" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = "Чтобы добавить новое упражнение к программе\nПиши:")
            bot.send_message(call.message.chat.id,
                    "\nheader:add_ex_to_program\
                    \nprogram:<Имя программы>\
                    \nex:<Имя уже созданного упражнения>\
                    \nday:<Номер дня>\
                    \ntext:<Описание подходов и повторений>")
        
        elif "Исключить упражнение из программы" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = "Чтобы исключить новое упражнение из программы,\n1)Выбери прогрмамму\n2)Выбери день\n3)Нажми на упражнение, которое хочешь удалить"
            )
            bot.send_message(call.message.chat.id, "Программы:", reply_markup = show_days_program_kb())

        elif "Удалить шаблон упражнения" in call.data:
            bot.edit_message_text(
                chat_id = call.message.chat.id,
                message_id = call.message.message_id,
                text = "Чтобы удалить упражнение\nПиши:"
            )
            bot.send_message(call.message.chat.id, "detele_ex_template:Имя упражнения")

if __name__ == "__main__":
    bot.polling(none_stop=True)
