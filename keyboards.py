from telebot import types
from sql.api import FindProgram

def user_kb():
	keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
	text_list = ["🔎Установить новую программу🔎", "🏋️️️️️️️️️️️️️Начать тренировку🏋️️️️️️️️️️"]
	row = [types.KeyboardButton(text = elem) for elem in text_list]
	keyboard.add(*row)
	return keyboard

def find_program_kb():
	programs = FindProgram()
	keyboard = types.InlineKeyboardMarkup()
	row = [types.InlineKeyboardButton(text=elem.name, callback_data=f"setprogram_{elem.id}") for elem in programs]
	keyboard.add(*row)
	return keyboard


def show_days_program_kb():
	programs = FindProgram()
	keyboard = types.InlineKeyboardMarkup()
	row = [types.InlineKeyboardButton(text=elem.name, callback_data=f"showdaysprogram_{elem.id}") for elem in programs]
	keyboard.add(*row)
	return keyboard

def split_by_days_kb(name_argument, program, days):
	keyboard = types.InlineKeyboardMarkup()
	row = [types.InlineKeyboardButton(text=f"День {elem}", callback_data=f"{name_argument}_{program.id}_{elem}") for elem in days]
	keyboard.add(*row)
	return keyboard

def admin_kb():
	keyboard = types.InlineKeyboardMarkup(row_width = 2)
	text_list = [
				 "Список всех пользователей",
				 "Список всех упражнений",
				 "Удалить мою программу",
				 "Создать новую программу ",
				 "️Cоздать новое упражнение",
				 "Удалить программу️",
				 "Добавить упражнение к программе",
				 "Исключить упражнение из программы",
				 "Удалить шаблон упражнения"
				 ]
	row = [types.InlineKeyboardButton(text = elem, callback_data=elem) for elem in text_list]
	keyboard.add(*row)
	return keyboard

def delete_ex_keyboard(program, exies):
	keyboard = types.InlineKeyboardMarkup(row_width = 2)
	row = [types.InlineKeyboardButton(text = f"❌{elem.ex_template.name}❌", callback_data = f"deleteex_{elem.id}_{program.id}") for elem in exies]
	keyboard.add(*row)
	return keyboard
