from functools import wraps
from sql.api import IsSuperUser
import telebot
from bot_config import bot

def only_superuser(function):
    @wraps(function)
    def wrap(message, *args, **kwargs):
        if IsSuperUser(message.chat.id):
            return function(message, *args, **kwargs)
        else:
            bot.send_message(message.chat.id, "Недостаточно прав!")
    return wrap