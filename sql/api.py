from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy import create_engine, and_
from sql.create_database import User, Program, Ex, Ex_template
from sql.sql_config import engine


Session = sessionmaker(bind = engine)
session = Session()


def CreateUser(chat_id, name):
    if not session.query(User).filter_by(chat_id = chat_id).first():
        new_user = User(chat_id=chat_id, fullname=name)
        session.add(new_user)
        session.commit()
        session.close()

def DeleteUser(chat_id):
    session.query(User).filter_by(chat_id = chat_id).delete()
    session.commit()

def FindUser(chat_id):
    user = session.query(User).filter_by(chat_id = chat_id).first()
    return user

def FindProgram():
    programs = session.query(Program)
    return programs
    
def UsersList():
    users = session.query(User)
    return users

def ExList():
    ex_templates = session.query(Ex_template)
    return ex_templates

def GoTrain(chat_id):
    program = FindUser(chat_id).program
    if program: 
        days = set([elem.day for elem in FindUser(chat_id).program.ex])
        return program, days
    else:
        return False, False

def SetProgram(chat_id, program_id):
    user_update = FindUser(chat_id)
    user_update.program = session.query(Program).filter_by(id = program_id).first()
    session.add(user_update)
    session.commit()
    return session.query(Program).filter_by(id = program_id).first().name
    
def GetTextDay(program_id, day):
    program = session.query(Program).filter_by(id = program_id).first()
    exies = (
        session.query(Ex)
            .join(Ex.program)
            .filter(Ex.day == day)
            .filter(Program.id == program_id)
    ).all()
    return program, exies

def DeleteMyProgram(chat_id):
    user = FindUser(chat_id)
    if user.program:
        session.delete(user.program)
        session.commit()

def DeleteProgram(program_name):
    program = session.query(Program).filter_by(name = program_name).first()
    if not program: return False
    else: 
        program.ex = []
        session.commit()
        session.delete(program)
        session.commit()
        return True

def CreateNewProgram(program_name):
    program = session.query(Program).filter_by(name = program_name).first()
    if program: return False
    else:
        new_program = Program(name = program_name)
        session.add(new_program)
        session.commit()
        return True

def CreateNewExTemplate(ex_name_template):
    ex_template = session.query(Ex_template).filter_by(name = ex_name_template).first()
    if ex_template: return False
    else:
        new_ex_template = Ex_template(name = ex_name_template)
        session.add(new_ex_template)
        session.commit()    
        return True

def AddExToProgram(data_list):
    program_name = data_list[0]
    ex_name      = data_list[1]
    day          = int(data_list[2])
    text         = data_list[3]
    program = session.query(Program).filter_by(name = program_name).first()
    ex = session.query(Ex_template).filter_by(name = ex_name).first()
    if not program: 
        return "Нет такой программы" 
    elif not ex: 
        return "Нет такого упражнения"
    elif not isinstance(day, int):
        return "День должен задаваться числом"
    elif day < 1 or day > 7: 
        return "День должен находится в диапазоне от 1 до 7"
    is_exist_ex = (
        session.query(Ex)
            .join(Ex.program)
            .join(Ex.ex_template)
            .filter(Ex_template.name == ex_name)
            .filter(Program.id == program.id)
            .filter(Ex.day == day)
    ).first()
    if is_exist_ex:
        return "Упражнение уже добавлено"
    new_ex = Ex(day = day, text = text)
    new_ex.ex_template = ex
    program.ex.append(new_ex)
    session.add(program)
    session.commit()
    return "Упражнение добавлено"

def ShowDaysProgram(program_id):
    program = session.query(Program).filter_by(id = program_id).first()
    days = set([elem.day for elem in program.ex])
    return program, days
    
def DeleteExFromProgram(data_list):
    ex_template_id = int(data_list[1])
    program_id = int(data_list[2])
    program = session.query(Program).filter_by(id = program_id).first()
    ex = session.query(Ex).filter_by(id = ex_template_id).first()
    old_ex = program.ex
    old_ex = [elem for elem in old_ex if elem != ex]
    program.ex = old_ex
    session.delete(ex)
    session.commit()
    
def IsSuperUser(chat_id):
    user = FindUser(chat_id)
    return user.superuser

def AddSuperUser(chat_id):
    user = FindUser(chat_id)
    user.superuser = True
    session.add(user)
    session.commit()

def DeleteExTemplate(ex_name):
    ex = session.query(Ex_template).filter_by(name = ex_name).first()
    if not ex:
        return f"Упражнение {ex_name} не существует"
    else:
        session.delete(ex)
        session.commit()
        return f"Упражнение {ex_name} удалено"
    