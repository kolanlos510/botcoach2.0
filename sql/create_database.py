from sqlalchemy import Column, Integer, String, ForeignKey, Table, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sql.sql_config import engine


Base = declarative_base()


association_table_program_ex = Table('association_table_program_ex', 
    Base.metadata,
    Column('ex_id', Integer, ForeignKey('ex.id')),
    Column('program_id', Integer, ForeignKey('program.id'))
)

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer)  
    fullname = Column(String)
    program_id = Column(Integer, ForeignKey("program.id"))
    superuser = Column(Boolean)
    program = relationship(
            "Program", 
            back_populates="user"
        )


class Program(Base):
    __tablename__ = 'program'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    user = relationship(
            "User",
            back_populates="program"
        )
    ex = relationship(
            "Ex",
            secondary=association_table_program_ex,
            back_populates="program",
        )


class Ex(Base):
    __tablename__ = "ex"
    id = Column(Integer, primary_key=True)
    text = Column(String)
    day = Column(Integer)
    ex_template_id = Column(Integer, ForeignKey("ex_template.id"))    
    ex_template = relationship(
            "Ex_template",
            back_populates="ex"
        )
    program = relationship(
            "Program",
            secondary=association_table_program_ex,
            back_populates="ex"
        )
    

class Ex_template(Base):
    __tablename__ = "ex_template"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    ex = relationship(
            "Ex",
            back_populates="ex_template",
            cascade="all, delete-orphan"
        )


Base.metadata.create_all(engine)
