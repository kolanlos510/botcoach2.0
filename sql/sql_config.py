from sqlalchemy import create_engine
import os

username = os.getenv('DB_USERNAME')
password = os.getenv('DB_PASSWORD') 
ip       = os.getenv('DB_HOST')
name     = os.getenv('DB_NAME')

engine   = create_engine(f'postgresql://{username}:{password}@{ip}/{name}')